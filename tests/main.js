class Generator {
    constructor(current, step) {
        this.current = current;
        this.step = step;
    }

    getNext() {
        const result = this.current;

        this.current += this.step;

        return result;
    }
}

function add(a, b) {
    return a + b;
}
QUnit.test( "math add test", function( assert ) {
    assert.ok( add(1, 2) === 3, "Passed!" );
});

QUnit.test( "generator test", function( assert ) {
    const generator = new Generator(0, 25);

    assert.ok( generator.getNext() === 0, "Passed!" );
    assert.ok( generator.getNext() === 25, "Passed!" );
    assert.ok( generator.getNext() === 50, "Passed!" );
});